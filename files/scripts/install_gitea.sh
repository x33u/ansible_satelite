#!/bin/sh
adduser --system \
        --shell /bin/bash \
        --gecos 'Git Version Control' \
        --group --disabled-password \
        --home /home/git \
        git 1>/dev/null

mkdir -p /var/lib/gitea/custom
mkdir -p /var/lib/gitea/data
mkdir -p /var/lib/gitea/log
chown -R git:git /var/lib/gitea/
chmod -R 750 /var/lib/gitea/
mkdir /etc/gitea
chown git:git /etc/gitea
chmod 770 /etc/gitea
wget -O gitea https://dl.gitea.io/gitea/{{ gitea_version }}/gitea-{{ gitea_version }}-linux-amd64
chmod +x gitea

export GITEA_WORK_DIR=/var/lib/gitea/
cp gitea /usr/local/bin/gitea

/usr/local/bin/gitea web -c /etc/gitea/app.ini &
sleep 5
killall gitea

chown git:git /etc/gitea/app.ini
