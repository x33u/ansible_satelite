#!/bin/sh
set -xe
rm -rf /var/www/html
git clone {{ git_hugo_www }} /var/www/html
chown -R www-data:www-data /var/www/html
exit
