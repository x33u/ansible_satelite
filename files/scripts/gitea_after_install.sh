#!/bin/sh
echo "[ui]" >> /etc/gitea/app.ini
echo "DEFAULT_THEME = arc-green" >> /etc/gitea/app.ini
chown root:git /etc/gitea
chmod 750 /etc/gitea
chmod 644 /etc/gitea/app.ini
systemctl restart gitea
rm /tmp/gitea_after_install.sh
