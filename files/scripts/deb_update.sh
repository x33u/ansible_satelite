#!/bin/bash

telegraf_version={{ telegraf_version }}
influxdb_version={{ influxdb_version }}
grafana_version={{ grafana_version }}

wget https://dl.influxdata.com/telegraf/releases/$telegraf_version
wget https://dl.influxdata.com/influxdb/releases/$influxdb_version
wget https://dl.grafana.com/oss/release/$grafana_version

mv $telegraf_version telegraf-latest.deb
mv $influxdb_version influxdb-latest.deb
mv $grafana_version grafana-latest.deb
