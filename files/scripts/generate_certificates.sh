#!/bin/bash

# -- nginx
if [ -e /etc/ssl/nginx.crt ]
then
    sleep 0
else
    openssl req -x509 -nodes -subj "/C=XX/ST=XX/L=XX/O=XX/OU=XX/CN=XX" -newkey rsa:4096 -keyout /etc/ssl/nginx.key -out /etc/ssl/nginx.crt -days 365
fi


# -- dh
if [ -e /etc/ssl/certs/dhparam.pem ]
then
    sleep 0
else
    openssl dhparam -out /etc/ssl/certs/dhparam.pem 4096
fi

# -- influxdb
if [ -e /etc/ssl/influxdb.crt ]
then
    sleep 0
else
    openssl req -x509 -nodes -subj "/C=XX/ST=XX/L=XX/O=XX/OU=XX/CN=XX" -newkey rsa:4096 -keyout /etc/ssl/influxdb.key -out /etc/ssl/influxdb.crt -days 365
    # chown influxdb:influxdb /etc/ssl/influxdb.*
fi
