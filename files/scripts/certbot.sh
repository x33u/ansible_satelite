#!/bin/bash
certbot certonly --noninteractive --webroot --agree-tos -m logcat@mailbox.org --redirect -d {{ server_name }} -w /var/www/html/
certbot certonly --noninteractive --webroot --agree-tos -m logcat@mailbox.org --redirect -d www.{{ server_name }} -w /var/www/html/
mkdir -p /var/www/git.{{ server_name }} 1>/dev/null
certbot certonly --noninteractive --webroot --agree-tos -m logcat@mailbox.org --redirect -d git.{{ server_name }} -w /var/www/git.{{ server_name }}
mkdir -p /var/www/mon.{{ server_name }} 1>/dev/null
certbot certonly --noninteractive --webroot --agree-tos -m logcat@mailbox.org --redirect -d mon.{{ server_name }} -w /var/www/mon.{{ server_name }}
